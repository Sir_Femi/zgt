require('dotenv').config();

const dbName = process.env.MYSQL_DATABASE;
const dbUser = process.env.MYSQL_USER;
const dbHost = process.env.MYSQL_HOSTNAME;
const dbDriver = process.env.MYSQL_DRIVER;
const dbPassword = process.env.MYSQL_PASSWORD;
const dbPort = process.env.MYSQL_PORT;


console.log(dbUser);

module.exports = {
  "development": {
    "username": dbUser,
    "password": dbPassword,
    "database": dbName,
    "host": dbHost,
    "port": dbPort,
    "dialect": dbDriver
  },
  "test": {
    "username": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "mysql"
  },
  "production": {
    "username": "root",
    "password": null,
    "database": "database_production",
    "host": "127.0.0.1",
    "dialect": "mysql"
  }
}
