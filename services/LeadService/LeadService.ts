import { InterestDao } from "./InterestDao";
import { LeadDao } from "./LeadDao";

export interface LeadRequestForm {
    email: string;
    phone: string;
    first_name: string;
    last_name: string;
    message: string;
}

export class LeadService {

    constructor(private leadDao: LeadDao, private interestDao: InterestDao) {}

    async getAllLeads() {
        return this.leadDao.getAll();
    }

    async deleteAll() {
        const deleteInterestPromises = this.interestDao.deleteAll();
        const deleteLeadPromises = this.leadDao.deleteAll();
        await Promise.all([deleteInterestPromises, deleteLeadPromises]);
    }

    async submitLeadRequest(doc: LeadRequestForm) {
        await this.createLead({
            email: doc.email,
            phone: doc.phone,
            first_name: doc.first_name,
            last_name: doc.last_name,
            message: doc.message
        });
        // const interestPromise = this.interestDao.create({
        //     lead_id: leadId,
        //     message: doc.message,
        // });
        //await Promise.all([leadPromise]);
    }

    async createLead(doc: LeadRequestForm) {
        const leadId = `${doc.phone}_${doc.email}`;
        const createLeadPromise = this.leadDao.findOrCreate(leadId, {
            id: leadId,
            ...doc,
        });
        const createInterestPromise = this.interestDao.create({
            lead_id: leadId,
            message: doc.message,
        });
        await Promise.all([createLeadPromise, createInterestPromise]);
    }

}