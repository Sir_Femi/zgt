import { InterestAttributes } from "../../models/interest";

export class InterestDao {

    constructor(private DB: { Interest: any }) { }

    async deleteAll() {
        return this.DB.Interest.destroy({where: {}});;
    }

    async create(doc: InterestAttributes) {
        return this.DB.Interest.create(doc);
    }

    async getAll() {
        return this.DB.Interest.findAll({
            order: ['createdAt', 'DESC']
        })
    }
}