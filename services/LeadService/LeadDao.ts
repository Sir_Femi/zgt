import { LeadAttributes } from "../../models/lead";

export class LeadDao {

    constructor(private DB: { Lead: any, Interest: any }) { }

    async deleteAll() {
        return this.DB.Lead.destroy({truncate: { cascade: true }});
    }

    async create(doc: LeadAttributes) {
        return this.DB.Lead.create(doc);
    }

    async getLeadById(id: string): Promise<LeadAttributes> {
        return this.DB.Lead.findByPk(id);
    }

    async findOrCreate(id: string, doc: LeadAttributes): Promise<LeadAttributes> {
        return this.DB.Lead.findOrCreate({
            where: { id: id },
            defaults: {
                ...doc,
            }
        });
    }

    async getAll(): Promise<LeadAttributes[]>{
        return this.DB.Lead.findAll({
            order: [
                ['createdAt', 'DESC']
            ],
            include: {
                model: this.DB.Interest
            }
        })
    }
}