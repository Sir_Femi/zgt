import express, { Request, Response, NextFunction } from 'express';
import bodyParser from 'body-parser';
import { NotFoundError, ApiError, InternalError } from './core/ApiError';
import Boom from 'boom';
import { ResponseFormat } from './core/ResponseFormat';
import morgan from 'morgan';
import db from './models';
import { leadRoute } from './routes/v1/lead/resource';
// import {leads} from './seeders/leads';
// import { interest } from './seeders/interest';

// import { leadRoute } from './routes/v1/lead/resource';
// import Lead from './models/lead';
// import Interest from './models/interest';

// const createLeads = () => {
//    leads.map((lead) => {
//         db.Lead.create(lead);
//    });
// };

// createLeads();

const createInterest = () => {
    // interest.map((interest) => {
    //      db.Interest.create(interest);
    // });
   
 };

//  db.Lead.findAll({
//   include: {
//     model: db.Interest
//   }
// }).then((results: object) => console.log(JSON.stringify(results)))
// .catch((err: any) => console.error(err));
 
// createInterest();


process.on('uncaughtException', (e) => {
  console.error(e.message);
});

const app = express();

app.set("port", process.env.PORT || 3001);

//this is more like a health check endpoint
app.get("/", (req, res) => {
  res.json({ status: "up" })
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan("tiny"));

// Routes
app.use('/api/v1/lead', leadRoute);

// catch 404 and forward to error handler
app.use((req, res, next) => next(new NotFoundError()));

// Middleware Error Handler
// eslint-disable-next-line @typescript-eslint/no-unused-vars
app.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  if (err instanceof ApiError) {
    ApiError.handle(err, res);
  } else {
    // if (getEnv().NODE_ENV === 'development') {
    //   console.error(err.message);
    //   const { output } = Boom.badRequest(err.message);
    //   return response.handleError(res, output);
    //   //return res.status(500).send(err.message);
    // }
    ApiError.handle(new InternalError(), res);
  }
});

//associations
// Lead.hasMany(Interest);
// Interest.belongsTo(Lead);

db.sequelize.sync().then(() => {
    console.log('all done');
}).catch((err: any) => {
    console.log({err});
});

export default app;