'use strict';
import {
  Model, Optional
} from 'sequelize';

export interface LeadAttributes {
  id: string;
  email: string;
  first_name: string;
  last_name: string;
  phone: string;
  interests?: [], //used when fetching associations (hasMany)
  createdAt?: Date,
  updatedAt?: Date
}

export interface LeadInput extends Optional<LeadAttributes, 'updatedAt' | 'createdAt'> { }
export interface LeadOuput extends Required<LeadAttributes> { };

module.exports = (sequelize: any, DataTypes: any) => {
  class Lead extends Model<LeadAttributes> implements LeadAttributes {
    id!: string;
    email!: string;
    first_name!: string;
    last_name!: string;
    phone!: string;
    
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */

    static associate(models: any) {
      // define association here
      Lead.hasMany(models.Interest, {
        foreignKey: 'lead_id'
      });
    }
  }
  Lead.init({
    id: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
      unique: true,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {isEmail: true},
    },
    first_name: {
      type:  DataTypes.STRING,
      allowNull: false,
    },
    last_name: {
      type:  DataTypes.STRING,
      allowNull: false,
    },
    phone: {
      type:  DataTypes.STRING,
      allowNull: false,
    },
    
  }, {
    timestamps: true,
    sequelize,
    modelName: 'Lead',
  });
  return Lead;
};