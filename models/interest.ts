'use strict';
import {
  Model, Optional
} from 'sequelize';
import { LeadAttributes } from './lead';

export interface InterestAttributes {
  id?: number;
  message: string;
  lead_id: string;
  Lead?: LeadAttributes; //used when fetching associations (belongsTo)
  createdAt?: Date,
  updatedAt?: Date
}

export interface InterestInput extends Optional<InterestAttributes, 'updatedAt' | 'createdAt'> { }
export interface InterestOuput extends Required<InterestAttributes> { };

module.exports = (sequelize: any, DataTypes: any) => {
  class Interest extends Model<InterestAttributes> implements InterestAttributes {
    id!: number;
    message!: string;
    lead_id!: string;

    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models: any) {
      // define association here
      Interest.belongsTo(models.Lead, {
        foreignKey: 'lead_id'
      })
    }
  }
  Interest.init({
    id: {
      type: DataTypes.INTEGER,
      //allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      unique: true,
    },
    message: {
      type: DataTypes.TEXT
    },
    lead_id: {
      type: DataTypes.STRING
    }
  }, {
    timestamps: true,
    sequelize,
    modelName: 'Interest',
  });
  return Interest;
};