
import { Router } from "express";
import { injectService } from "../../../middlewares/serviceMiddleware";
import { createLead, deleteAll, getAllLeads, submitRequest } from "./controller";

const router = Router();

router.post('/create', injectService, createLead);

router.post('/submit_request', injectService, submitRequest);

router.get('/all', injectService, getAllLeads);

router.delete('/all', injectService, deleteAll);

export { router as leadRoute };
