import { LeadService } from "./services/LeadService/LeadService";
import DB from './models';
import { LeadDao } from "./services/LeadService/LeadDao";
import { InterestDao } from "./services/LeadService/InterestDao";

 export interface Service {
    //environment: ENV,
}

export interface ServiceContainer extends Service {
    leadService: LeadService,
}

const createContainer = () => {
    const leadDao = new LeadDao(DB);
    const interestDao = new InterestDao(DB);
    const leadService = new LeadService(leadDao, interestDao);

    const container: ServiceContainer = {
        leadService
    }
    return container;
};

const service = createContainer();

//to use the service container anywhere else out of context (using the this keyword)
export const getService = () => {
    return service;
};